"""main app to deliver (fast)api functionality using plain sql"""
import time
from typing import Optional

import psycopg2
from fastapi import FastAPI, HTTPException, status
from psycopg2.extras import RealDictCursor
from pydantic import BaseModel
from starlette.responses import Response
from starlette.status import HTTP_204_NO_CONTENT

# database connection and cursor
while True:
    try:
        conn = psycopg2.connect(host='db', database='fastapi', user='postgres',
                                password='postgres', cursor_factory=RealDictCursor)
        cursor = conn.cursor()
        break
    except Exception as error:
        print(f'Connection failed ({error}).')
        time.sleep(2)

app = FastAPI()


class Post(BaseModel):
    """How should a new Post look like."""
    title: str
    content: str
    published: bool = True
    rating: Optional[int] = None


@app.get("/posts")
def get_posts():
    """List all posts ordered by id (ASC)."""
    query = "SELECT * FROM posts order by id;"
    cursor.execute(query)
    my_posts = cursor.fetchall()
    return {"data": my_posts}


@app.get("/posts/latest")
def get_latest_post():
    """Returns the latest post."""
    query = "SELECT * FROM posts ORDER BY id DESC LIMIT 1"
    cursor.execute(query)
    latest_post = cursor.fetchall()
    return{"data": latest_post}


@app.get("/posts/{post_id}")
def get_specifig_post(post_id: int):
    """Returns a specific post."""
    query = "SELECT * FROM posts WHERE id = %s"
    cursor.execute(query, (str(post_id),))
    post = cursor.fetchone()
    if not post:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'Post with post_id {post_id} was not found!')
    return {'data': post}


@app.post("/posts", status_code=status.HTTP_201_CREATED)
def create_post(post: Post):
    """Create a new post using the Post class."""
    query = "INSERT INTO posts (title, content, published) VALUES (%s, %s, %s) RETURNING *"
    cursor.execute(query, (post.title, post.content, post.published))
    new_post = cursor.fetchone()
    conn.commit()
    return {"data": new_post}


@app.delete("/posts/{post_id}", status_code=status.HTTP_204_NO_CONTENT)
def del_specific_post(post_id: int):
    """Delete a specific post."""
    query = "SELECT id FROM posts WHERE id = %s"
    cursor.execute(query, (str(post_id),))
    index = cursor.fetchone()
    if index is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f'Post with post_id {post_id} does not exist!')
    query = "DELETE FROM posts WHERE id = %s"
    cursor.execute(query, (str(post_id),))
    conn.commit()
    return(Response(status_code=HTTP_204_NO_CONTENT))


@app.put("/posts/{post_id}")
def update_specific_post(post_id: int, post: Post):
    """Update a specific post."""
    query = "SELECT id FROM posts WHERE id = %s"
    cursor.execute(query, (str(post_id),))
    index = cursor.fetchone()
    print(post_id)

    if index is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f'Post with post_id {post_id} does not exist!')

    post_dict = post.dict()
    post_dict['post_id'] = str(post_id)

    # update post
    query = "UPDATE posts SET title=%s, content=%s, published=%s WHERE id = %s"
    cursor.execute(query, (post_dict['title'], post_dict['content'],
                   post_dict['published'], post_dict['post_id']))
    conn.commit()

    # return updated post to api
    query = "SELECT * FROM posts WHERE id = %s"
    cursor.execute(query, (str(post_id),))
    update_post = cursor.fetchone()
    return {'data': update_post}
