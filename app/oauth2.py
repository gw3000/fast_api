"""OAUTH2 Module for the API"""
from datetime import datetime, timedelta

from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer
from jose import JWTError, jwt
from sqlalchemy.orm import Session

from . import models, schemas
from .config import settings
from .database import get_db

oath2_scheme = OAuth2PasswordBearer(tokenUrl='login')


def create_access_token(data: dict):
    """Generate the access token"""
    to_encode = data.copy()

    expire = datetime.utcnow() + timedelta(minutes=settings.ACCESS_TOKEN_EXPIRE)
    to_encode.update({'exp': expire})

    enoceded_jwt = jwt.encode(
        to_encode,
        settings.SECRET_KEY,
        algorithm=settings.ALGORITHM)

    return enoceded_jwt


def verify_access_token(token: str, credentials_exception):
    """Verify the token using the secret key and the algorithm"""
    try:
        payload = jwt.decode(
            token, settings.SECRET_KEY, algorithms=[
                settings.ALGORITHM])
        user_id: str = payload.get('user_id')

        if user_id is None:
            raise credentials_exception
        token_data = schemas.TokenData(id=user_id)
    except JWTError as error:
        print(error)
        raise credentials_exception

    return token_data


def get_current_user(
        token: str = Depends(oath2_scheme),
        data_base: Session = Depends(get_db)):
    """Get current user using the token"""
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail='Could not validate credentials!',
        headers={'WWW-Authenticate': 'Bearer'})

    token = verify_access_token(token, credentials_exception)
    user = data_base.query(models.User).filter(
        models.User.id == token.id).first()

    return user
