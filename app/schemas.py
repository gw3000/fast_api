"""Module for all API SCHEMAS"""
from datetime import datetime
from typing import Optional

from pydantic import BaseModel, EmailStr
from pydantic.types import conint


# -----------------------------------------------------------------------------
# User
# -----------------------------------------------------------------------------


class UserCreate(BaseModel):
    """Base Model of user"""
    email: EmailStr
    password: str


class UserResponse(BaseModel):
    """Response Model of user"""
    id: int
    email: EmailStr
    created_at: datetime

    class Config:
        """Subclass Config"""
        orm_mode = True


# -----------------------------------------------------------------------------
# Login
# -----------------------------------------------------------------------------


class UserLogin(BaseModel):
    """User Authentication Class"""
    email: EmailStr
    password: str


# -----------------------------------------------------------------------------
# Posts
# -----------------------------------------------------------------------------


class PostBase(BaseModel):
    """The Base Class of posts."""
    title: str
    content: str
    published: bool = True


class PostCreate(PostBase):
    """Which data should be created"""
    pass


class PostUpdate(PostBase):
    """Which data should be updated"""
    pass


class PostResponse(PostBase):
    """Post Resonse Class"""
    id: int
    created_at: datetime
    owner_id: int
    # owner: sql-> select * from posts inner join users where users.id
    owner: UserResponse

    class Config:
        """Orm_mode will tell Pydantix to read
        the data even its not a dict"""
        orm_mode = True


# -----------------------------------------------------------------------------
# Votes
# -----------------------------------------------------------------------------


class PostVotes(BaseModel):
    """Post with Votes number"""
    Post: PostResponse
    votes: int

    class Config:
        """Orm_mode will tell Pydantix to read
        the data even its not a dict"""
        orm_mode = True


class VoteCreate(BaseModel):
    """Vote Creating Class"""
    post_id: int
    dir: conint(le=1)


class Token(BaseModel):
    """Token Model"""
    access_token: str
    token_type: str


class TokenData(BaseModel):
    """Token Data"""
    id: Optional[str] = None
