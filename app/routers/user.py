"""Users route"""
from typing import List
from fastapi import Depends, HTTPException, Response, APIRouter, status
from sqlalchemy.orm import Session
from .. import models, schemas, utils
from ..database import get_db



router = APIRouter(
    prefix='/users',
    # title in docs / webfrontend
    tags=['Users']
)


@router.post('/', status_code=status.HTTP_201_CREATED,
             response_model=schemas.UserResponse)
def create_user(user: schemas.UserCreate,
                data_base: Session = Depends(get_db)):
    """Create a user with email and password."""
    user.password = utils.hashed_password(user.password)
    query = data_base.query(
        models.User).filter(
        models.User.email == user.email)
    if query.first() is not None:
        raise HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            detail=f'User with email {user.email} already in use!'
        )
    new_user = models.User(**user.dict())
    data_base.add(new_user)
    data_base.commit()
    data_base.refresh(new_user)
    return new_user


@router.get('/', response_model=List[schemas.UserResponse])
def get_users(data_base: Session = Depends(get_db)):
    """Return all users"""
    users = data_base.query(models.User).all()
    return users


@router.get('/{user_id}', response_model=schemas.UserResponse)
def get_user(user_id: int, data_base: Session = Depends(get_db)):
    """Returns a specific user depended on the id."""
    user = data_base.query(models.User).filter(
        models.User.id == user_id).first()
    if user is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f'User with ID {user_id} does not exist!'
        )
    return user


@router.delete("/{user_id}", status_code=status.HTTP_204_NO_CONTENT)
def del_user(user_id: int, data_base: Session = Depends(get_db)):
    """Delete a specific user depending on the id."""
    user = data_base.query(models.User).filter(models.User.id == user_id)

    if user.first() is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f'User with ID {user_id} does not exist!')
    user.delete(synchronize_session=False)
    data_base.commit()
    response = Response(status_code=status.HTTP_204_NO_CONTENT)
    return response
