"""Posts route"""
from typing import List, Optional

from fastapi import APIRouter, Depends, HTTPException, Response, status
from sqlalchemy import func
from sqlalchemy.orm import Session

from .. import models, oauth2, schemas
from ..database import get_db


router = APIRouter(
    prefix='/posts',
    # title in docs / webfrontend
    tags=['Posts']
)


@router.get("/", response_model=List[schemas.PostVotes])
def get_posts(data_base: Session = Depends(get_db),
              _current_user=Depends(oauth2.get_current_user),
              limit: int = 10,
              skip: int = 0,
              search: Optional[str] = ''
              ):
    """Return all posts (with filter and limit options)"""
    #  data = data_base.query(models.Post).filter(models.Post.owner_id==current_user.id).all()
    #  data = data_base.query( models.Post).filter(models.Post.owner_id == current_user.id).filter( or_( models.Post.title.contains(search), models.Post.content.contains(search))).limit(limit).offset(skip).all()
    query = data_base.query(
        models.Post,
        func.count(
            models.Vote.post_id).label('votes')).join(
        models.Vote,
        models.Vote.post_id == models.Post.id,
        isouter=True).group_by(
                models.Post.id).filter(
        models.Post.title.contains(search)).limit(limit).offset(skip)

    data = query.all()
    return data


@router.get("/{post_id}", response_model=schemas.PostVotes)
def get_post(post_id: int,
             data_base: Session = Depends(get_db),
             current_user=Depends(oauth2.get_current_user)):
    """Returns a specific post depended on the id."""
    post_query = data_base.query(models.Post).filter(
        models.Post.id == post_id)

    if post_query.first().owner_id != current_user.id:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Not authorized to perform requested action")
    #  data = post_query.filter(models.Post.id == post_id).first()
    data = data_base.query(
        models.Post,
        func.count(
            models.Vote.post_id).label('votes')).join(
        models.Vote,
        models.Vote.post_id == models.Post.id,
        isouter=True).group_by(
                models.Post.id).filter(models.Post.id == post_id).first()

    return data


@router.post("/", status_code=status.HTTP_201_CREATED,
             response_model=schemas.PostResponse)
def create_post(post: schemas.PostCreate,
                data_base: Session = Depends(get_db),
                current_user=Depends(oauth2.get_current_user)):
    """Create a new post."""
    # use the owner id from the token, unpack the post dict with **
    new_post = models.Post(owner_id=current_user.id, **post.dict())
    data_base.add(new_post)
    data_base.commit()
    data_base.refresh(new_post)
    return new_post


@router.delete("/{post_id}", status_code=status.HTTP_204_NO_CONTENT)
def del_post(post_id: int,
             data_base: Session = Depends(get_db),
             current_user=Depends(oauth2.get_current_user)):
    """Delete a specific post depending on the id."""
    post_query = data_base.query(models.Post).filter(
        models.Post.id == post_id)

    if post_query.first() is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f'Post with ID {post_id} does not exist!')

    if post_query.first().owner_id != current_user.id:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Not authorized to perform requested action!"
        )

    post_query.delete(synchronize_session=False)
    data_base.commit()
    data = Response(status_code=status.HTTP_204_NO_CONTENT)
    return data


@router.put("/{post_id}", response_model=schemas.PostResponse)
def update_post(post_id: int, updated_post: schemas.PostCreate,
                data_base: Session = Depends(get_db),
                current_user: int = Depends(oauth2.get_current_user)):
    """Update a specific depending on its ID post."""
    post_query = data_base.query(models.Post).filter(models.Post.id == post_id)

    if post_query.first() is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"post with id: {post_id} does not exist")

    if post_query.first().owner_id != current_user.id:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Not authorized to perform requested action")

    post_query.update(updated_post.dict(), synchronize_session=False)

    data_base.commit()

    return post_query.first()
