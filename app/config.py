"""Configuration Module"""
from pydantic import BaseSettings


class Settings(BaseSettings):
    """Read values from .env file"""
    DB_HOST: str
    DB_PORT: str
    DB_USER: str
    DB_PASSWORD: str
    DB_NAME: str
    SECRET_KEY: str
    ALGORITHM: str
    ACCESS_TOKEN_EXPIRE: int

    class Config:
        env_file = ".env"


settings = Settings()
