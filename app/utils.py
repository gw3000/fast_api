"""Simple Utilities Module"""
from passlib.context import CryptContext

password_context = CryptContext(schemes=['bcrypt'], deprecated='auto')


def hashed_password(password: str):
    """Hashing a password using bcrypt"""
    return password_context.hash(password)


def verify(plain_password, hased_password):
    """Verify a plain password and its hashed combatant"""
    return password_context.verify(plain_password, hased_password)
