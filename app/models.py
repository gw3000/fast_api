"""The Models Module"""
from sqlalchemy import Boolean, Column, Integer, String, Text
from sqlalchemy.orm import relationship
from sqlalchemy.sql.expression import text
from sqlalchemy.sql.schema import ForeignKey
from sqlalchemy.sql.sqltypes import TIMESTAMP

from .database import Base


class Post(Base):
    """Blueprint for Posts"""
    __tablename__ = 'posts'
    id = Column(Integer, primary_key=True, nullable=False, index=True)
    title = Column(String, index=True, nullable=False)
    content = Column(Text, nullable=False)
    published = Column(Boolean, server_default='True', nullable=False)
    created_at = Column(TIMESTAMP(timezone=True),
                        nullable=False, server_default=text('now()'))
    owner_id = Column(Integer, ForeignKey("users.id", ondelete="CASCADE"),
                      nullable=False)
    owner = relationship("User")


class Vote(Base):
    """How should Votestable look like"""
    __tablename__ = 'votes'
    user_id = Column(
        Integer,
        ForeignKey(
            "users.id",
            ondelete="CASCADE"),
        primary_key=True)
    post_id = Column(
        Integer,
        ForeignKey(
            "posts.id",
            ondelete="CASCADE"),
        primary_key=True)


class User(Base):
    """Building plan for Users."""
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True, nullable=False, index=True)
    email = Column(String, nullable=False, unique=True)
    password = Column(String, nullable=False)
    created_at = Column(TIMESTAMP(timezone=True),
                        nullable=False, server_default=text('now()'))
