"""add content column to posts table

Revision ID: 82ce5b6d2d0f
Revises: 2734eb63bb08
Create Date: 2021-12-08 13:58:42.329410

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '82ce5b6d2d0f'
down_revision = '2734eb63bb08'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('posts', sa.Column('content', sa.Text(), nullable=False))


def downgrade():
    op.drop_column('posts', 'content')
