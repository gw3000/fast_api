"""Post Table

Revision ID: 2734eb63bb08
Revises: 
Create Date: 2021-12-08 12:02:11.872979

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '2734eb63bb08'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('posts', sa.Column('id', sa.Integer(), nullable=False,
                    primary_key=True), sa.Column('title', sa.String(), nullable=False))


def downgrade():
    op.drop_table('post')
