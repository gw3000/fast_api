## dev environment
- docker
- vscode


## http-methods

https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods

## api best practices

https://swagger.io/resources/articles/best-practices-in-api-design/

## http-status-codes

https://developer.mozilla.org/en-US/docs/Web/HTTP/Status

## generate secret key

```bash
openssl rand -base64 54
```

## Alembic

### Initialisation

To initialize the database accoring to the schema declarations you can do something like:

```bash
alembic revision --autogenerate -m "Database initialization"
```

... if you have a clean Databse with no data and no tables.

### Build the Tables

To build the database with its tables execute:

```bash
alembic upgrade head
```

Now you should have a clean Database with no data in it.

## CORS

### origins

Allows only the given domains in the list:
```bash
origins = ['https://myhost.org', 'https://myotherhost.de']
```

... or from everywhere

```bash
origins = ['*']
```
